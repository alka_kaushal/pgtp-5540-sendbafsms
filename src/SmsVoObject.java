import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;
import com.games24x7.framework.mq.MQMessage;


public class SmsVoObject implements MQMessage{

	private Long mobileNumber;
	private Long userId;
	private String message;
	private int smsType;
	public static String mqPropsFilePath = "/home/alka/Documents/PGTP-5540_SMS_CMQueue/src/mq.props";
	private static PropsFileBasedConfiguration mainConfig;
	
	public Long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getSmsType() {
		return smsType;
	}
	public void setSmsType(int smsType) {
		this.smsType = smsType;
	}
	
	
	@Override
	public String toString() {
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath);
		/*MQFrameworkFactory.init(mainConfig);*/
		if(mainConfig.getIntValue("SendMessageType")==1){
		return "SmsVoObject [mobileNumber=" + mobileNumber + ", userId="
				+ userId + ", message=" + message + ", smsType="
				+ smsType + "]";
	}
	else{
		return "SmsVoObject [mobileNumber=" + mobileNumber + ", userId="
				+ userId + ", message=" + message + "]";
		
	}
}
}
