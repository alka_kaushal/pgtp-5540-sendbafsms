
import com.games24x7.common.domainObject.SMSVO;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;


public class CMQueuePublisher {
	
	public static String mqPropsFilePath = "/home/alka/Documents/PGTP-5540_SMS_CMQueue/src/mq.props";
	private static PropsFileBasedConfiguration mainConfig;
	private static String CMQueueName = "cmQueue";

	public static void main(String args[])
	{	
		try
		{
			mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath);
			MQFrameworkFactory.init(mainConfig);
			MQFrameworkFactory.getFramework().registerQueuePublisher(CMQueueName);
			CMQueuePublisher.createSMSObject();

		}
		catch(Exception e){
			
		e.printStackTrace();
		}
		}
	
	
	public static void createSMSObject(){
		
		SMSVO smsVoMessage  = new SMSVO();
		smsVoMessage.setMobileNumber(mainConfig.getLongValue("mobileNumber"));
		smsVoMessage.setUserId(mainConfig.getLongValue("userId"));
		smsVoMessage.setMessage(mainConfig.getStringValue("message"));	
		smsVoMessage.setSmsType(mainConfig.getIntValue("smsType"));
		
		
		System.out.println("SMSVOObject to be published: "+smsVoMessage);
		
		MQFrameworkFactory.getFramework().publishToQueue( CMQueueName, smsVoMessage);
		
		
	}	
}
