import com.games24x7.framework.mq.MQMessage;
import com.games24x7.framework.mq.MQMessageHandler;

public class CMConsumerHandler implements MQMessageHandler {

	@Override
	public void handleMessage(MQMessage arg0) {

		System.out.println("CMQueue MQMessage : " + arg0.toString());

	}

	@Override
	public void handleMessage(String message) {
		
			System.out.println("CM String: " + message.toString());

	}

}
